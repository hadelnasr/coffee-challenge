CREATE DATABASE `coffee_machines`;

USE `coffee_machines`;

CREATE TABLE IF NOT EXISTS `coffee_machines` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `machine_size` ENUM('SMALL', 'LARGE', 'ESPRESSO') NOT NULL,
  `model` ENUM('BASE', 'PREMIUM', 'DELUXE') NOT NULL,
  `water_line_compatible` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `coffee_flavors` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `flavor` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `coffee_pack_sizes` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `size` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `coffee_pods_product_types` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `pod_size` ENUM('SMALL', 'LARGE', 'ESPRESSO') NOT NULL,
  `flavor` SMALLINT UNSIGNED NOT NULL,
  `pack_size` SMALLINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_coffee_pods_product_types_coffee_flavors_idx` (`flavor` ASC),
  INDEX `fk_coffee_pods_product_types_coffee_pack_sizes1_idx` (`pack_size` ASC),
  CONSTRAINT `fk_coffee_pods_product_types_coffee_flavors`
    FOREIGN KEY (`flavor`)
    REFERENCES `coffee_flavors` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_coffee_pods_product_types_coffee_pack_sizes1`
    FOREIGN KEY (`pack_size`)
    REFERENCES `coffee_pack_sizes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;