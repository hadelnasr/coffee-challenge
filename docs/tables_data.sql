
--
-- Dumping data for table `coffee_flavors`
--

LOCK TABLES `coffee_flavors` WRITE;
/*!40000 ALTER TABLE `coffee_flavors` DISABLE KEYS */;
INSERT INTO `coffee_flavors` VALUES (1,'VANILLA'),(2,'CARAMEL'),(3,'PSL'),(4,'MOCHA'),(5,'HAZELNUT');
/*!40000 ALTER TABLE `coffee_flavors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `coffee_machines`
--

LOCK TABLES `coffee_machines` WRITE;
/*!40000 ALTER TABLE `coffee_machines` DISABLE KEYS */;
INSERT INTO `coffee_machines` VALUES (1,'small machine','SMALL','BASE',0),(2,'small machine','SMALL','PREMIUM',0),(3,'small machine','SMALL','DELUXE',1),(4,'large machine','LARGE','BASE',0),(5,'large machine','LARGE','PREMIUM',1),(6,'large machine','LARGE','DELUXE',1),(7,'espresso machine','ESPRESSO','BASE',0),(8,'espresso machine','ESPRESSO','PREMIUM',0),(9,'espresso machine','ESPRESSO','DELUXE',1);
/*!40000 ALTER TABLE `coffee_machines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `coffee_pack_sizes`
--

LOCK TABLES `coffee_pack_sizes` WRITE;
/*!40000 ALTER TABLE `coffee_pack_sizes` DISABLE KEYS */;
INSERT INTO `coffee_pack_sizes` VALUES (1,'1 dozen (12)'),(2,'3 dozen (36)'),(3,'5 dozen (60)'),(4,'7 dozen (84)');
/*!40000 ALTER TABLE `coffee_pack_sizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `coffee_pods_product_types`
--

LOCK TABLES `coffee_pods_product_types` WRITE;
/*!40000 ALTER TABLE `coffee_pods_product_types` DISABLE KEYS */;
INSERT INTO `coffee_pods_product_types` VALUES (1,'small coffee pod','SMALL',1,1),(2,'small coffee pod','SMALL',1,3),(3,'small coffee pod','SMALL',2,1),(4,'small coffee pod','SMALL',2,3),(5,'large coffee pod','LARGE',1,1),(6,'large coffee pod','LARGE',1,3),(7,'large coffee pod','LARGE',2,1),(8,'large coffee pod','LARGE',2,3),(10,'espresso pod','ESPRESSO',1,2),(11,'espresso pod','ESPRESSO',1,4);
/*!40000 ALTER TABLE `coffee_pods_product_types` ENABLE KEYS */;
UNLOCK TABLES;