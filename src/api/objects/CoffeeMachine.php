<?php

namespace Coffee;

class CoffeeMachine extends BaseObject
{

    // object properties
    public $id;
    public $name;
    public $machine_size;
    public $model;
    public $water_line_compatible;

    public function __construct($db)
    {
        parent::__construct($db, "coffee_machines");
    }

    /**
     * read all the coffee machines
     * @return mixed
     */
    public function read()
    {
        // select all query
        $query = "SELECT id, name, machine_size, model, water_line_compatible
            FROM
                " . $this->table_name . " 
            ORDER BY id DESC";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    /**
     * return specific coffee machines
     * @param $searchParams array
     * @return mixed
     */
    public function search($searchParams = [])
    {
        $query = "SELECT id, name, machine_size, model, water_line_compatible FROM " . $this->table_name;
        if(count($searchParams) > 0) {
            $query .= " WHERE ";
            $i = 0;
            foreach ($searchParams as $key => $value) {
                if($i == 0) {
                    $query .= " $key = '$value'";
                    $i ++;
                } else {
                    $query .= " AND $key = '$value'";
                }
            }
        }
        $query .= " ORDER BY id DESC";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }
}