<?php

namespace Coffee;

class BaseObject
{
    public $conn;
    public $table_name;

    // constructor with $db as database connection
    public function __construct($db, $tableName)
    {
        $this->conn = $db;
        $this->table_name = $tableName;
    }
}