<?php

namespace Coffee;

class CoffeePod extends BaseObject
{

    // object properties
    public $id;
    public $name;
    public $pod_size;
    public $flavor;
    public $pack_size;

    public function __construct($db)
    {
        parent::__construct($db, "coffee_pods_product_types");
    }

    /**
     * read all the coffee machines
     * @return mixed
     */
    public function read()
    {
        // select all query
        $query = "SELECT p.id, p.name, p.pod_size, ps.size as pack_size, f.flavor FROM " . $this->table_name . " p ";
        $query .= " INNER JOIN coffee_flavors f on f.id = p.flavor ";
        $query .= " INNER JOIN coffee_pack_sizes ps on ps.id = p.pack_size ";
        $query .= " ORDER BY id DESC";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    /**
     * return specific coffee machines
     * @param $searchParams array
     * @return mixed
     */
    public function search($searchParams = [])
    {
        $query = "SELECT p.id, p.name, p.pod_size, ps.size as pack_size, f.flavor FROM " . $this->table_name . " p ";
        $query .= " INNER JOIN coffee_flavors f on f.id = p.flavor ";
        $query .= " INNER JOIN coffee_pack_sizes ps on ps.id = p.pack_size ";
        if (count($searchParams) > 0) {
            $query .= " WHERE ";
            $i = 0;
            foreach ($searchParams as $key => $value) {
                if ($i == 0) {
                    $query .= " p.$key = '$value'";
                    $i++;
                } else {
                    $query .= " AND p.$key = '$value'";
                }
            }
        }

        $query .= " ORDER BY id DESC";
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }
}