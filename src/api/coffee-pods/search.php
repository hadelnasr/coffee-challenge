<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

require __DIR__ . '/../../../vendor/autoload.php';

// include database and object files
include_once '../config/core.php';
include_once '../config/database.php';
include_once '../objects/CoffeePod.php';

// instantiate database and coffee_machine object
$database = new Database();
$db = $database->getConnection();

// initialize object
$coffeePod = new Coffee\CoffeePod($db);

$searchParams = [];
isset($_GET["pod_size"]) ? $searchParams['pod_size'] = $_GET["pod_size"] : "";
isset($_GET["flavor"]) ? $searchParams['flavor'] = $_GET["flavor"] : "";
isset($_GET['pack_size']) ? $searchParams['pack_size'] = $_GET["pack_size"] : "";

// query coffee_machines
$stmt = $coffeePod->search($searchParams);
$num = $stmt->rowCount();

// check if more than 0 record found
if ($num > 0) {

    // coffee_machines array
    $coffee_pods_arr = array();
    $coffee_pods_arr["records"] = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $coffee_machine_item = array(
            "id" => $row['id'],
            "name" => $row['name'],
            "pod_size" => $row['pod_size'],
            "flavor" => $row['flavor'],
            "pack_size" => $row['pack_size']
        );
        array_push($coffee_pods_arr["records"], $coffee_machine_item);
    }

    $coffee_pods_arr["total"] = count($coffee_pods_arr["records"]);

    // set response code - 200 OK
    http_response_code(200);

    // show coffee_machines data
    echo json_encode($coffee_pods_arr);
} else {
    // set response code - 404 Not found
    http_response_code(404);

    // tell the user no coffee_machines found
    echo json_encode(
        array("message" => "No coffee_pods found.")
    );
}