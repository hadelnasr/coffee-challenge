<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

require __DIR__ . '/../../../vendor/autoload.php';

// include database and object files
include_once '../config/database.php';
include_once '../objects/CoffeePod.php';

// instantiate database and coffee pod object
$database = new Database();
$db = $database->getConnection();

// initialize object
$coffeePod = new Coffee\CoffeePod($db);
// query coffee machines
$stmt = $coffeePod->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if ($num > 0) {

    // coffee pods array
    $coffeePods_arr = array();
    $coffeePods_arr["records"] = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $coffeePod_item = array(
            "id" => $row['id'],
            "name" => $row['name'],
            "pod_size" => $row['pod_size'],
            "flavor" => $row['flavor'],
            "pack_size" => $row['pack_size']
        );

        array_push($coffeePods_arr["records"], $coffeePod_item);
    }

    $coffeePods_arr["total"] = count($coffeePods_arr["records"]);
    // set response code - 200 OK
    http_response_code(200);

    // show coffee pods data in json format
    echo json_encode($coffeePods_arr);
} else {

    // set response code - 404 Not found
    http_response_code(404);

    // tell the user no coffee pods found
    echo json_encode(
        array("message" => "No Coffee pods found.")
    );
}