<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

require __DIR__ . '/../../../vendor/autoload.php';

// include database and object files
include_once '../config/core.php';
include_once '../config/database.php';
include_once '../objects/CoffeeMachine.php';

// instantiate database and coffee_machine object
$database = new Database();
$db = $database->getConnection();

// initialize object
$coffeeMachine = new Coffee\CoffeeMachine($db);

$searchParams = [];
isset($_GET["size"]) ? $searchParams['machine_size'] = $_GET["size"] : "";
isset($_GET["model"]) ? $searchParams['model'] = $_GET["model"] : "";
isset($_GET['waterCompatible']) && is_numeric($_GET["waterCompatible"]) ? $searchParams['water_line_compatible'] = $_GET["waterCompatible"] : "";

// query coffee_machines
$stmt = $coffeeMachine->search($searchParams);
$num = $stmt->rowCount();

// check if more than 0 record found
if ($num > 0) {

    // coffee_machines array
    $coffee_machines_arr = array();
    $coffee_machines_arr["records"] = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $coffee_machine_item = array(
            "id" => $row['id'],
            "name" => $row['name'],
            "machine_size" => $row['machine_size'],
            "model" => $row['model'],
            "water_line_compatible" => $row['water_line_compatible']
        );
        array_push($coffee_machines_arr["records"], $coffee_machine_item);
    }
    $coffee_machines_arr["total"] = count($coffee_machines_arr["records"]);

    // set response code - 200 OK
    http_response_code(200);

    // show coffee_machines data
    echo json_encode($coffee_machines_arr);
} else {
    // set response code - 404 Not found
    http_response_code(404);

    // tell the user no coffee_machines found
    echo json_encode(
        array("message" => "No coffee_machines found.")
    );
}