<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

require __DIR__ . '/../../../vendor/autoload.php';

// include database and object files
include_once '../config/database.php';
include_once '../objects/CoffeeMachine.php';

// instantiate database and coffee machine object
$database = new Database();
$db = $database->getConnection();

// initialize object
$coffeeMachine = new Coffee\CoffeeMachine($db);
// query coffee machines
$stmt = $coffeeMachine->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if ($num > 0) {

    // coffee machines array
    $coffeeMachines_arr = array();
    $coffeeMachines_arr["records"] = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $coffeeMachine_item = array(
            "id" => $row['id'],
            "name" => $row['name'],
            "machine_size" => $row['machine_size'],
            "model" => $row['model'],
            "water_line_compatible" => $row['water_line_compatible']
        );

        array_push($coffeeMachines_arr["records"], $coffeeMachine_item);
    }
    $coffeeMachines_arr["total"] = count($coffeeMachines_arr["records"]);

    // set response code - 200 OK
    http_response_code(200);

    // show coffee machines data in json format
    echo json_encode($coffeeMachines_arr);
} else {

    // set response code - 404 Not found
    http_response_code(404);

    // tell the user no coffee machines found
    echo json_encode(
        array("message" => "No Coffee machines found.")
    );
}