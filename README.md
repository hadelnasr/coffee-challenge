# Coffee Challenge

An e-commerce mobile application for line of coffee machines and custom coffee pods; there will be two
apis to serve 2 screens: one screen to display coffee machines and one screen to display coffee pods. On the coffee machines screen, the user may filter by
product type and water line. On the coffee pods screen, the user may filter by product type, coffee flavor, and pack size.
## Getting Started

First of all find the ERD file in the docs folder called coffee-project.mwb to see the tables relationships

### Prerequisites

```
PHP 7.2 with Mysql installed
```

### Installing

```
Run composer install
Then composer dump-autoload -o
Create db first & user to have read privileges on it and it's tables
Edit the database connection params in the /coffee-challenge/src/api/config/database.php
Open mysql terminal and run:
* SOURCE /coffee-challenge/docs/tables_schema.sql
* SOURCE /coffee-challenge/docs/tables_data.sql
```

### APIs

#### Coffee Machines APIs
```
get all coffee-machines: 
http://localhost/coffee-challenge/src/api/coffee-machines/read.php
```

```
get all coffee-machines by query params: 
http://localhost/coffee-challenge/src/api/coffee-machines/search.php?size=LARGE&model=BASE&waterCompatible=0
```

```
get all coffee-machines by query params size: 
http://localhost/coffee-challenge/src/api/coffee-machines/search.php?size=LARGE
```

```
get all coffee-machines by query params model: 
http://localhost/coffee-challenge/src/api/coffee-machines/search.php?model=BASE
```

```
get all coffee-machines by query params water compatible: 
http://localhost/coffee-challenge/src/api/coffee-machines/search.php?waterCompatible=0
```

#### Coffee Pods APIs

```
get all coffee-pods: 
http://localhost/coffee-challenge/src/api/coffee-pods/read.php
```

```
get all coffee-pods by query params: 
http://localhost/coffee-challenge/src/api/coffee-pods/search.php?pod_size=SMALL&flavor=2&pack_size=1
```

```
get all coffee-pods by query params pod size: 
http://localhost/coffee-challenge/src/api/coffee-pods/search.php?pod_size=SMALL
```

```
get all coffee-pods by query params flavor: 
http://localhost/coffee-challenge/src/api/coffee-pods/search.php?flavor=2
```

```
get all coffee-pods by query params pack size: 
http://localhost/coffee-challenge/src/api/coffee-pods/search.php?pack_size=1
```



### Unit Tests
```
Run from the root terminal
./vendor/bin/phpunit tests
```
## Authors

* **Haytham Adel** - *Initial work* - [haytham_einshams@hotmail.com](mailto:haytham_einshams@hotmail.com)

## License

This project is licensed under the MIT License